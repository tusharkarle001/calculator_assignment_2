let history_arr = [];
let history_shown = false;
let sci_cal_shown = false;

function ansget() {

    try {


        let val = document.getElementById("equation").value;
        if (val != "PI") {
            var ans1 = eval(val.replace("PI", "*3.14"));
        } else {
            var ans1 = eval(val.replace("PI", "3.14"));
        }


        history_arr.push(document.getElementById("equation").value + "=" + ans1);
        // console.log(history_arr);
        document.getElementById("equation").value = ans1;
        if (history_shown == true) {
            show_history();
        }

    } catch (error) {
        console.log(error);
        document.getElementById("equation").value = "Invalid Input";
    }
}

function clicked(a) {

    let prev_data = document.getElementById("equation").value;
    prev_data = prev_data.trim();
    // console.log(prev_data);
    let new_data = a.trim();
    let final_data = prev_data.concat("", new_data);
    document.getElementById("equation").value = final_data;
    // console.log(final_data);
}

function clear_input() {

    document.getElementById("equation").value = "";
}

function clear_prev() {

    let prev = document.getElementById("equation").value;
    prev = prev.substr(0, prev.length - 1);
    document.getElementById("equation").value = prev;

}


function show_history() {

    if (history_shown == false) {
        let container = document.getElementById("history ");
        for (let i = history_arr.length - 1; i >= 0; i--) {
            var paragraph = document.createElement("h3");
            paragraph.setAttribute("style", "font-size:1.5rem;padding:2px 30px;border: 2px solid white;margin:7px 10px; ;border-radius:20px;color:white;font-family: 'Roboto', sans-serif;letter-spacing:0.1rem;");
            paragraph.innerHTML = history_arr[i];
            container.appendChild(paragraph);
            console.log("printed");
        }
        history_shown = true;
        clear_input();
    } else {
        const myNode = document.getElementById("history ");
        while (myNode.firstChild) {
            myNode.removeChild(myNode.lastChild);
        }
        // console.log("deleted");
        history_shown = false;
    }



}


function scientific() {

    if (!sci_cal_shown) {
        let sci_div = document.getElementsByClassName("scientific");
        for (let i = 0; i < sci_div.length; i++) {
            sci_div[i].setAttribute("style", "display:flex;");
        }
        sci_cal_shown = true;
        document.getElementById("sci_btton ").innerHTML = "Normal";
        let elem = document.getElementsByTagName("button");
        // console.log(elem);
        for (let i = 1; i < elem.length; i++) {
            elem[i].setAttribute("style", "height:8.75vh;");
        }

    } else {
        let sci_div = document.getElementsByClassName("scientific");
        for (let i = 0; i < sci_div.length; i++) {
            sci_div[i].setAttribute("style", "display:none;");
        }
        sci_cal_shown = false;

        let elem = document.getElementsByTagName("button");
        // console.log(elem);
        for (let i = 1; i < elem.length; i++) {
            elem[i].setAttribute("style", "height:12.5vh;");
        }


        //change the button name
        document.getElementById("sci_btton ").innerHTML = "Scientific";
    }
}


function scientific_calc(val12) {

    try {
        // console.log("step1");
        // getting the final input from the input bar
        let val = document.getElementById("equation").value;
        if (val != "PI") {
            var ans = eval(val.replace("PI", "*(22/7)"));
        } else {
            var ans = eval(val.replace("PI", "(22/7)"));
        }

        // console.log("step2", ans);

        // evaluate the equation using switch case
        switch (val12) {
            case "Sin":
                console.log(ans);
                var final_ans = Math.sin(ans);
                final_ans = final_ans.toFixed(3);
                console.log(`${ans} of the sin is =${final_ans}`);
                break;

            case "Cos":
                var final_ans = Math.cos(ans);
                final_ans = final_ans.toFixed(3);
                break;
            case "Tan":
                var final_ans = Math.tan(ans);
                final_ans = final_ans.toFixed(3);
                break;

            case "log":
                var final_ans = Math.log(ans);
                final_ans = final_ans.toFixed(3);
                break;
            case "Sqrt":
                var final_ans = Math.sqrt(ans);
                final_ans = final_ans.toFixed(3);
                break;
            case "reverse":
                var final_ans = 1 / ans;
                break;
            case "Factorial":
                if (ans == 1 || ans == 0) {
                    var final_ans = 1;
                } else {
                    let fact = 1;
                    for (let i = 1; i < ans; i++) {
                        fact = fact * i;
                    }
                    var final_ans = fact;

                }

                final_ans = final_ans.toFixed(3);
                break;


            default:
                console.log("Invalid Input");
                break;
        }
        //adding the history
        history_arr.push(val12 + " ( " + document.getElementById("equation").value + " ) =" + final_ans);

        // console.log(history_arr);

        document.getElementById("equation").value = final_ans;


        if (history_shown == true) {
            show_history();
        }

    } catch (error) {
        console.log(error);
        document.getElementById("equation").value = "Invalid Input";
    }

}